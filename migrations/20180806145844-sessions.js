'use strict';

var dbm;
var type;
var seed;

exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function(db) {
  return db.createTable('sessions', {
    id: { type: 'int', primaryKey: true },
    userId: 'int',
  });
};

exports.down = function(db) {
  return db.dropTable('sessions');
};

exports._meta = {
  'version': 1
};
