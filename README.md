# Nodejs REST base project with Postgres

Nodejs base to create API's using Postgres. Without framework!

[![NPM version][shield-npm]](#)
[![Node.js version support][shield-node]](#)
[![MIT licensed][shield-license]](#)

```sh
npm install && npm start

# Production?
# use PM2 or similar to start node bin/www
```

## Table of Contents
  * [Requirements](#requirements)
  * [Usage](#usage)
  * [Contributing](#contributing)
  * [Support and Migration](#support-and-migration)
  * [License](#license)

## Requirements

This project requires the following to run:

  * [Node.js][node] 9.10.1
  * [npm][npm] 5.8.0

## Usage

```sh
# Development server
npm install

# setup env variables described in .env.example
# create a local database described in `config/database.json`
npm run migrate up

# start the server
npm start

# Production server
npm i --production
# Using PM2 or another nodejs process manager, start `node index.js`

# Tests
npm t

# Test coverage
npm run coverage

# Aditional tasks
npm run
```

## Contributing

To contribute, clone this repo locally and commit your code on a separate
branch. Please write unit tests for your code, and run the linter before
opening a pull-request and follow `.editorconfig` :)

```sh
npm t && npm run lint
```


License
-------

This base project is licensed under the [MIT](#) license.
Copyright &copy; 2018, Lester Montenegro

[node]: https://nodejs.org/
[npm]: https://www.npmjs.com/
[shield-license]: https://img.shields.io/badge/license-MIT-blue.svg
[shield-node]: https://img.shields.io/badge/node.js%20support-9.10.1-brightgreen.svg
[shield-npm]: https://img.shields.io/badge/npm-v5.8.0-blue.svg
