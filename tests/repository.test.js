const tap = require('tap');
const helper = require('../repository');

const table = [
  {
    input: { db: { query: () => ''}, table: 'users' },
    output: ''
  }
];

table.map(item => {
  let repo = helper(item.input);
  tap.equal(repo.all(), item.output);
  tap.equal(repo.first(), item.output);
  tap.equal(repo.last(), item.output);
  tap.equal(repo.find(), item.output);
  tap.equal(repo.create(), item.output);
  tap.equal(repo.update(), item.output);
  tap.equal(repo.remove(), item.output);
});
