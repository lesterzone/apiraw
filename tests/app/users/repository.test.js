const tap = require('tap');
const helper = require('../../../app/users/repository');

const table = [
  {
    input: { db: { query: () => '', table: 'foo' } },
    output: ''
  }
];

table.map(item => {
  const result = helper(item.input);

  tap.equal(result.all(), item.output);
});
