const tap = require('tap');
const helper = require('../../../app/users/validate');

const table = [
  {
    input: { email: 'f', password: 'f' },
    output: { email: 'f', password: 'f' }
  },
  {
    input: {},
    output: { email: 'Email is required', password: 'Password is required' }
  }
];

table.map(item => {
  helper(item.input)
    .then(data => tap.deepEqual(data, item.output))
    .catch(error => tap.deepEqual(error, item.output));
});
