const tap = require('tap');
const helper = require('../../../app/users/create');

const table = [
  {
    input: {
      db: {
        query: () => Promise.resolve(),
      },
      next: () => Promise.resolve({ email: 'f', password: 'o' }),
      request: { body: { email: 'f', password: 'o' } }
    },
    output: { email: 'f', password: 'o' }
  },
  {
    input: {
      db: {
        query: () => Promise.resolve(),
      },
      next: () => Promise.reject({ email: 'Email is required', password: 'Password is required' }),
      request: { body: { email: 'f', password: 'o' } }
    },
    output: { email: 'Email is required', password: 'Password is required' }
  },
];

table.map(item => {
  helper(item.input)
    .then(data => {
      tap.deepEqual(data, item.output);
    })
    .catch(error => {
      tap.deepEqual(error, item.output);
    });
});
