const tap = require('tap');
const helper = require('../../../app/users/user');

const table = [
  {
    input: { email: 'f', password: 'o' },
    output: { email: 'f', password: 'o' }
  },
  {
    input: { email: 'f' },
    output: { email: 'f', password: '' }
  },
  {
    input: {},
    output: { email: '', password: '' }
  },
  {
    output: { email: '', password: '' }
  }
];

table.map(item => {
  let actual = helper(item.input);
  tap.deepEqual(actual, item.output);
});
