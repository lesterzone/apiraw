const tap = require('tap');
const helper = require('../../middlewares/verifyToken');
const baseContext = require('./baseContext');

const table = [
  {
    input: {
      ...baseContext,
      request: { headers: { authorization: 'foo' } }
    },
    output: 'foo'
  },
  {
    input: {
      ...baseContext,
      request: { headers: { authorization: '' } }
    },
    output: 403
  }
];

table.map(item => {
  helper(item.input)
    .then(() => {
      tap.equal(item.input.token, item.output);
    })
    .catch(error => {
      tap.equal(error.statusCode, 403);
    })
});
