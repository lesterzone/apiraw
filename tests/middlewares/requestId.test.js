const tap = require('tap');
const helper = require('../../middlewares/requestId');
const baseContext = require('./baseContext');

const table = [
  {
    input: {
      ...baseContext,
    },
    output: 0
  },
  {
    input: {
      ...baseContext,
    },
    output: 0
  }
];

table.map(item => {
  helper(item.input)
    .then(() => {
      let current = item.input.requestId > 0;
      tap.equal(current, true);
    })
    .catch(error => {
      tap.equal(error.statusCode, 403);
    })
});
