module.exports = {
  next: (context) => Promise.resolve(context),
  readOnly: function readonly(key, value, context) {
    Object.defineProperty(context, key, {
      value: value,
      writable: false
    });
  }
}
