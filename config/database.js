const { Pool } = require('pg');
let pool;

module.exports = {
  query: (text, params, callback) => {
    return pool.query(text, params, callback);
  },
  connect: (options) => {
    pool = new Pool(options);
    return pool.connect();
  }
};
