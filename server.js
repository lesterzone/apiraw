const url = require('url');
const execute = require('./execute');
const { logRequest, requestId } = require('./middlewares');

function readOnly(key, value, context) {
  Object.defineProperty(context, key, {
    value: value,
    writable: false
  });
}

module.exports = (options) => function handler(request, response) {
  const context = {};


  // https://stackoverflow.com/questions/7757337/defining-read-only-properties-in-javascript#7757493
  Object.defineProperty(context, 'readOnly', {
    value: readOnly,
    writable: false
  });

  const query = url.parse(request.url, true).query;

  response.setHeader('content-type', 'application/json');

  context.readOnly('next', (context) => Promise.resolve(context), context);
  context.readOnly('request', request, context);
  context.readOnly('response', response, context);
  context.readOnly('queryString', query, context);
  context.readOnly('db', options.db, context);
  context.readOnly('log', console.log, context);

  const path = request.url.split('?')[0];
  const key = request.method + ' ' + path;


  if (options.routes[key]) {
    return execute(context, [logRequest, requestId])
      .then(() => {
        options.routes[key](context);
      });
  }

  // just end the request!
  response.end();
};

