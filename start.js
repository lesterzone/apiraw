const spawn = require('child_process').spawn;
const nodemon = require('nodemon');
const log = console.log;
const fs = require('fs');

const options = {
  script: 'bin/www',
  verbose: true,
  inpect: true,
  debug: true,
};

function start() {
  log(`Process started pid: ${process.pid}`);
}

function test(file) {
  const isTest = file.includes('.test.js');
  const options = {
    stdio: 'inherit',
    shell: true,
  };

  if (isTest) {
    return spawn('node_modules/.bin/tap ' + file, options);
  }

  const path = file.replace(__dirname, './tests').replace('.js', '.test.js');

  return fs.stat(path, function (err) {
    if (err) {
      return;
    }

    return spawn('node_modules/.bin/tap ' + path, options);
  });
}

function lint(file) {
  const options = {
    stdio: 'inherit',
    shell: true,
  };

  return spawn('node_modules/.bin/eslint ' + file, options);
}

function restart(files) {
  const file = files[0];
  if (!file) {
    return;
  }

  lint(file);
  test(file);
}

nodemon(options)
  .on('start', start)
  .on('crash', log)
  .on('restart', restart);
