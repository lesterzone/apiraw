const repository = require('../../repository');

module.exports = function userRepository(options) {
  let content = {
    db: options.db,
    table: 'users'
  };

  return repository(content);
};
