const create = require('./create');
const execute = require('../../execute');

module.exports = {
  'POST /users': (context) => execute(context, [create]),
};
