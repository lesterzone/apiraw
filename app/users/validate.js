// const validators = {
//   present: (key, value) => {
//     if (!value) {
//       return `${key} is required`;
//     }

//     return '';
//   },
//   email: (value) => {
//     return '';
//   }
// }
module.exports = function validate(object) {

  return new Promise((resolve, reject) => {
    let errors = {};

    if (!object.email) {
      errors['email'] = 'Email is required';
    }


    if (!object.password) {
      errors['password'] = 'Password is required';
    }

    if (Object.keys(errors).length) {
      return reject(errors);
    }

    return resolve(object);
  });
};
