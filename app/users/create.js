const repository = require('./repository');
const user = require('./user');
const validate = require('./validate');

module.exports = function create(context) {
  let repo = repository(context);
  let model = user(context.request.body);

  return validate(model)
    .then(() => repo.create(model))
    .then(data => {
      context.data = data;
      return context.next(data);
    });
};
