module.exports = function (options = {}) {
  const { email = '', password = '' } = options;

  return {
    email,
    password,
  };
};
