function internal(options) {
  const { db } = options;

  function create(query = {}) {
    return db.query(query);
  }

  function update(query) {
    return db.query(query);
  }

  function remove(query) {
    return db.query(query);
  }

  function all() {
    return db.query('SELECT * FROM ' + options.table);
  }

  function find(query) {
    return db.query(query);
  }

  function first() {
    let query = {
      text: 'SELECT * FROM ' + options.table + ' ORDER BY DESC LIMIT 1',
    };

    return db.query(query);
  }

  function last() {
    let query = {
      text: 'SELECT * FROM ' + options.table + ' ORDER BY ASC LIMIT 1',
    };

    return db.query(query);
  }

  return Object.freeze({
    create,
    update,
    remove,
    all,
    find,
    first,
    last,
  });
}

module.exports = function repository(options) {
  return internal(options);
};
