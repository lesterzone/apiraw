const { respondError } = require('./middlewares');

module.exports = function execute(context, middlewares) {
  function apply(previous, current) {
    return previous
      .then(() => current(context))
      .catch(error => respondError(error, context));
  }

  return middlewares.reduce(apply, context.next(context));
};
