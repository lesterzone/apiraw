module.exports = function replyError(error, context) {
  context.log(error);
  context.response.statusCode = error.statusCode || 500;
  context.response.end(JSON.stringify({ error: error.message }));
}
