module.exports = function respond(context) {
  context.response.end(JSON.stringify(context.data || {}));
};
