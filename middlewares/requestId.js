module.exports = function requestId(context) {
  context.requestId = Date.now();
  return context.next();
};
