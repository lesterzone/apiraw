module.exports = function logRequest(context) {
  context.log(Date.now());
  context.log(context.request.headers['user-agent']);
  context.log(context.request.method + ' ' + context.request.url);
  return context.next();
}
