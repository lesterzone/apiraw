module.exports = function verifyToken(context) {
  const token = context.request.headers.authorization;
  if (!token) {
    let error = Error('Unauthorized');
    error.statusCode = 403;

    return context.next(Promise.reject(error));
  }

  context.readOnly('token', token, context);
  return context.next();
};
